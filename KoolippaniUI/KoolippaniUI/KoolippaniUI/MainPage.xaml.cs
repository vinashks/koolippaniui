﻿using KoolippaniUI.Modal;
using KoolippaniUI.Page;
using KoolippaniUI.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace KoolippaniUI
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void OnPostJobClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new PostJob());
        }

        async void OnFindJobClicked(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new FindJobPage());
            //RestClient restClient = new RestClient();
            //List<Job> jobList = await restClient.GetAllJobs();
            //foreach (var job in jobList)
            //{
            //    DisplayAlert("a", job.description, "Cancel");
            //}
        }

        async void OnMyHomeClicked(object sender, EventArgs args)
        {
        }
    }
}
