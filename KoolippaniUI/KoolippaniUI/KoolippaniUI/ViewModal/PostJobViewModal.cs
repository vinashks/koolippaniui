﻿using KoolippaniUI.Modal;
using KoolippaniUI.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace KoolippaniUI.ViewModal
{
    class PostJobViewModal : INotifyPropertyChanged
    {
        RestClient _restClient = new RestClient();
        List<JobType> _jobTypeList = new List<JobType> ();
        public List<JobType> JobTypeList
        {
            get { return _jobTypeList; }
            set
            {
                if (_jobTypeList != value)
                {
                    _jobTypeList = value;
                    OnPropertyChanged();
                }
            }
        }

        JobType selectedJobType;
        public JobType SelectedJobType
        {
            get {
                /*if (SelectedJobType == null)
                {
                    SelectedJobType = _jobTypeList.GetEnumerator().Current;
                }*/
                return selectedJobType; }
            set
            {
                if (selectedJobType != value)
                {
                    selectedJobType = value;
                    OnPropertyChanged();
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public async Task GetData()
        {
            List<JobType> jobTypes = await _restClient.GetJobTypes();
            foreach (var jobType in jobTypes)
            {
                //selJobType.Items.Add(jobType.name);
                _jobTypeList.Add(jobType);
            }
        }
    }
}

