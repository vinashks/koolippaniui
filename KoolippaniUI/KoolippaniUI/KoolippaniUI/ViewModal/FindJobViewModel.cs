﻿using KoolippaniUI.Modal;
using KoolippaniUI.Web;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace KoolippaniUI.ViewModal
{
    class FindJobViewModel : BaseViewModel
    {
        RestClient _restClient = new RestClient();
        List<JobType> _jobTypeList = new List<JobType>();
        public ListView _listView = new ListView();

        public List<JobType> JobTypeList
        {
            get { return _jobTypeList; }
            set
            {
                if (_jobTypeList != value)
                {
                    SetProperty(ref _jobTypeList, value);
                }
            }
        }

        JobType selectedJobType;
        public JobType SelectedJobType
        {
            get
            {
                return selectedJobType;
            }
            set
            {
                if (selectedJobType != value)
                {
                    SetProperty(ref selectedJobType, value);
                    JobList = _jobs.Where(s => s.jobTypeId == selectedJobType.id).ToList();
                }
            }
        }


        List<Job> _jobList = new List<Job>();
        public List<Job> JobList
        {
            get { return _jobList; }
            set
            {
                if (_jobList != value)
                {
                    SetProperty(ref _jobList, value);
                }
            }
        }

        Job selectedJob;
        public Job SelectedJob
        {
            get
            {
                return selectedJob;
            }
            set
            {
                if (selectedJob != value)
                {
                    SetProperty(ref selectedJob, value);
                }
            }
        }

        public async Task GetJobTypeData()
        {
            List<JobType> jobTypes = await _restClient.GetJobTypes();
            foreach (var jobType in jobTypes)
            {
                _jobTypeList.Add(jobType);
            }
        }
        List<Job> _jobs = new List<Job>();
        public async Task GetJobData()
        {
            _jobs = await _restClient.GetAllJobs();
            foreach (var job in _jobs)
            {
                _jobList.Add(job);
            }
        }
    }
}
