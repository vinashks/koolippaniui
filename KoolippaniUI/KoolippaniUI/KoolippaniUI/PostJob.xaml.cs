﻿using KoolippaniUI.Modal;
using KoolippaniUI.ViewModal;
using KoolippaniUI.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace KoolippaniUI
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostJob : ContentPage
    {
        RestClient _restClient = new RestClient();
        List<JobType> _jobTypes = new List<JobType> ();
        //private Dictionary<int, string> _jobTypes = new Dictionary<int, string>();
        //private KeyValuePair<int, string> _selectedJobType = new KeyValuePair <int, string> ();

        public  PostJob()
        {
            //_jobTypes.Add(100, "abc");
            //_jobTypes.Add(101, "abcd");
            InitializeComponent();

            //LoadJobTypes();

            /*this.BindingContext = new PostJobViewModal
            {
                JobTypeList = GetJobTypeList()
            };*/
        }

        protected override async void OnAppearing()
        {
            //await LoadJobTypes();
            var bc = new PostJobViewModal();
            await bc.GetData();
            BindingContext = bc;
        }

        /*private List<JobType> GetJobTypeList()
        {
            List<JobType> JobTypeList = new List<JobType>();
            JobType jobType = new JobType();
            jobType.id = 1;
            jobType.name = "name1";
            jobType.description = "Name one";
            JobTypeList.Add(jobType);
            return JobTypeList;
        }*/

        private async Task< List<JobType>>  GetJobTypeList() 
        {
            return await _restClient.GetJobTypes();
        }
        
        /*
        public List<KeyValuePair<int, string>> JobTypeList
        {
            get => _jobTypes.ToList();
        }

        public KeyValuePair<int, string> SelectedJobType
        {
            get => _selectedJobType;
            set => _selectedJobType = value;
        }*/

        async Task LoadJobTypes()
        {
            List<JobType> jobTypes =  await _restClient.GetJobTypes();
            foreach (var jobType in jobTypes)
            {
                //selJobType.Items.Add(jobType.name);
                _jobTypes.Add(jobType);
            }
            //_jobTypes.Add(12, "hello");
            //OnPropertyChanged();
        }

        async void OnSubmitClicked(object sender, EventArgs args)
        {
            await Navigation.PopAsync();
        }
    }
}