﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KoolippaniUI.Modal
{
    class JobType
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
    }
}
