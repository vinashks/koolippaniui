﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KoolippaniUI.Modal
{
    class Job
    {
        public long id { get; set; }
        public string description { get; set; }
        public double minimumCost { get; set; }
        public double maximumCost { get; set; }
        public int minEstimatedTime { get; set; }
        public int maxEstimatedTime { get; set; }
        public int timeMeasurement { get; set; }
        public int jobTypeId { get; set; }
        public int employerId { get; set; }
        public int bidderCount { get; set; }
        public int workerId { get; set; }
        public DateTime? postedOn { get; set; }
        public DateTime? completedOn { get; set; }
        public int status { get; set; }
        public int addressId { get; set; }
    }
}
