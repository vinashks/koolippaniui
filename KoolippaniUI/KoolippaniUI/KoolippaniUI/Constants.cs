﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KoolippaniUI
{
    class Constants
    {
        //public const string REST_SERVER = "http://10.0.2.2:63516/api/";//"http://localhost:63516/api/";
        public const string REST_SERVER = "http://10.0.2.2/KoolippaniApp/api/"; //"http://localhost/KoolippaniApp/api/";
        public const string EP_JOBTYPE = REST_SERVER + "jobtype";
        public const string EP_JOB = REST_SERVER + "job";
        //public const string EP_JOBTYPE = "http://10.0.2.2:8080/next-working-day?after=abc"; //"https://api.openweathermap.org/data/2.5/weather";//REST_SERVER + "jobtype";
        public const string EP_JOB_JOB = REST_SERVER + "job";
    }
}
