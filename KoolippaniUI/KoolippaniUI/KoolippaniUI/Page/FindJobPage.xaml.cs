﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using KoolippaniUI.Modal;
using KoolippaniUI.ViewModal;
using KoolippaniUI.Web;

namespace KoolippaniUI.Page
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FindJobPage : ContentPage
	{
        RestClient _restClient = new RestClient();
        List<JobType> _jobTypes = new List<JobType>();
        List<Job> _jobs = new List<Job>();
        public FindJobPage ()
		{
			InitializeComponent ();
		}

        protected override async void OnAppearing()
        {
            //await LoadJobTypes();
            var bc = new FindJobViewModel();
            await bc.GetJobTypeData();
            await bc.GetJobData();
            BindingContext = bc;
        }
        private async Task<List<JobType>> GetJobTypeList()
        {
            return await _restClient.GetJobTypes();
        }

        /*
        public List<KeyValuePair<int, string>> JobTypeList
        {
            get => _jobTypes.ToList();
        }

        public KeyValuePair<int, string> SelectedJobType
        {
            get => _selectedJobType;
            set => _selectedJobType = value;
        }*/

        async Task LoadJobTypes()
        {
            List<JobType> jobTypes = await _restClient.GetJobTypes();
            foreach (var jobType in jobTypes)
            {
                //selJobType.Items.Add(jobType.name);
                _jobTypes.Add(jobType);
            }
            //_jobTypes.Add(12, "hello");
            //OnPropertyChanged();
        }


        async Task LoadJobs()
        {
            // need to optimize by passing paramter based filter query
            List<Job> jobs = await _restClient.GetAllJobs();
            foreach (var job in jobs)
            {
                //selJobType.Items.Add(jobType.name);
                _jobs.Add(job);
            }
            //_jobTypes.Add(12, "hello");
            //OnPropertyChanged();
        }
    }
}