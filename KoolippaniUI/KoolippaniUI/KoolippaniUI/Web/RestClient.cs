﻿using KoolippaniUI.Modal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace KoolippaniUI.Web
{
    class RestClient
    {
        HttpClient _httpClient = new HttpClient();
        public async Task <List <JobType>> GetJobTypes()
        {
            List<JobType> jobTypes = new List<JobType>();
            //var uri = new Uri(Constants.EP_JOBTYPE);
            var uri = new Uri(string.Format(Constants.EP_JOBTYPE, string.Empty));
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await _httpClient.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                jobTypes = JsonConvert.DeserializeObject<List<JobType>>(content);
            }

            return jobTypes;
        }

        public async Task<List<Job>> GetAllJobs() 
        {
            List<Job> jobs = new List<Job>();
            var uri = new Uri(string.Format(Constants.EP_JOB, string.Empty));
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await _httpClient.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                jobs = JsonConvert.DeserializeObject<List<Job>>(content);
            }
            return jobs;
        }
    }
}
